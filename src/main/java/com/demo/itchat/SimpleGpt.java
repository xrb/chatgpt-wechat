package com.demo.itchat;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import cn.zhouyafeng.itchat4j.beans.BaseMsg;
import cn.zhouyafeng.itchat4j.face.IMsgHandlerFace;
import com.demo.common.GptApi;
import com.jfinal.kit.PropKit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 谢公子 153993778@qq.com
 * @date 2023-02-10
 * @Desc 获取微信信息并通过Api获取chatApi信息，将信息返回
 */
public class SimpleGpt implements IMsgHandlerFace {

    private static Logger LOG = LoggerFactory.getLogger(SimpleGpt.class);

    /**
     * 测试解析返回数据
     * @param args
     */
    public static void main(String[] args) {
        String str = "{\"id\":\"cmpl-6i1Jhis4SZliWekjZaKa6aDkqbhh3\",\"object\":\"text_completion\",\"created\":1675949513,\"model\":\"text-davinci-003\",\"choices\":[{\"text\":\"位于湖北省武汉市中山大道黄鹤楼。黄鹤楼始建于明末清初，是中国重要的古建筑之一。其历史悠久，曾为宋江定义革命而闻名。这座古建筑始建于1381年，主体结构内容按照四方庭园式格局，主要景观分布在池中。建筑的艺术风格以宋朝的建筑色彩为主，集传统优雅与新式现代主义之美。2002年，黄鹤楼被中国政府列为第一批国家文物保护单位，是武汉市乃至湖北省最著名的旅游景点之一，也是重要的历史文化景点。\",\"index\":0,\"logprobs\":null,\"finish_reason\":\"stop\"}],\"usage\":{\"prompt_tokens\":9,\"completion_tokens\":399,\"total_tokens\":408}}";
        JSONObject jsonObject2 = JSONUtil.parseObj(str);
        JSONArray jsonArray = new JSONArray(jsonObject2.get("choices").toString());
        JSONObject subImageInfoObject = jsonArray.getJSONObject(0);

        System.out.println(subImageInfoObject.get("text"));
    }

    @Override
    public String textMsgHandle(BaseMsg msg) {
        String text = msg.getText();
        return new GptApi().getChatGpt(text);
    }

    @Override
    public String picMsgHandle(BaseMsg msg) {
        return "暂未实现方法";
    }

    @Override
    public String voiceMsgHandle(BaseMsg msg) {
        return "暂未实现方法";
    }

    @Override
    public String viedoMsgHandle(BaseMsg msg) {
        return "暂未实现方法";
    }

    @Override
    public String nameCardMsgHandle(BaseMsg msg) {
        return "暂未实现方法";
    }

    @Override
    public void sysMsgHandle(BaseMsg msg) {

    }

    @Override
    public String verifyAddFriendMsgHandle(BaseMsg msg) {
        return "暂未实现方法";
    }

    @Override
    public String mediaMsgHandle(BaseMsg msg) {
        return "暂未实现方法";
    }
}
