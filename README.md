###  **基于chatGPT** 

将个人微信化身GPT机器人， 项目基于[itchat4j](https://github.com/yaphone/itchat4j)，[jfinal](https://gitee.com/jfinal/jfinal?_from=gitee_search)开发。

欢迎大家start!!!!!!

### 功能

机器人私聊回复

### 注意事项

项目仅供娱乐，滥用可能有微信封禁的风险，请勿用于商业用途。
请注意收发敏感信息，本项目不做信息过滤。

### 项目部署

1. maven项目，直接idea打开即可
2. 根据自己实际情况，调整配置里的内容
修改 demo-config-dev.txt

2.1 其中配置文件参考下边的配置文件说明。

- gpt_api = sk-
- api_url = https://api.openai.com/v1/completions
- timeout = 60000
- max_tokens = 521
- temperature = 0.9
- itchat_path = D://itchat4j

2.2配置文件说明

- gpt_api ：openai api_key
- timeout ：会话超时时间，默认60秒，单位毫秒。
- max_tokens: GPT响应字符数，最大2048，默认值512。max_tokens会影响接口响应速度，字符越大响应越慢。
- temperature: GPT热度，0到1，默认0.9。数字越大创造力越强，但更偏离训练事实，越低越接近训练事实
- itchat_path: 存放接收的文件位置

3. 在D://itchat4j创建login,pic,viedo,voice文件夹，文件夹用来放对应的文件，登录二维码在login先


### 快速开始

### 启动项目
1. 这里是列表文本将项目导入idea
2. mvn clean package
3. target\chatgpt_wechat_demo-release\chatgpt_wechat_demo 目录下 start.bat 

chatGpt key获取教程
首先需要在chatgpt官网:https://openai.com/

注册一个账号，这里我就不多说了，注册完成之后登录即可。 https://beta.openai.com/overview

然后在右上角的 View Api KeY 进行创建查看
